import { MailerService } from '@nestjs-modules/mailer';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';

@Controller('email')
export class EmailController {
  constructor(private mailService: MailerService) {}

  @Get('plain-text-email')
  async plainTextEmail(@Query('toemail') toemail) {
    await this.mailService.sendMail({
      to: toemail,
      from: 'alotinbehardieu@gmail.com',
      subject: 'Simple Plain Text',
      text: 'Welcome to NetJs Email demo',
    });
    return 'success';
  }

  @Post('html-email')
  async postHtmlEmail(@Body() payload) {
    await this.mailService.sendMail({
      to: payload.toemail,
      from: 'alotinbehardieu@gmail.com',
      subject: 'Simple Plain Text',
      template: 'superhero',
      context: {
        superHero: payload,
      },
    });
    return 'success';
  }
}
